<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="stylesheet"
	href="<c:url value="/resources/css/bootstrap.min.css" />">
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.1.10.2.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/student.js" />"></script>

<style type="text/css">
table tr, th, td {
	text-align: center;
}
</style>
<title><spring:message code="student.label"/></title>
</head>
<body>
	<div class="panel panel-default">
		<div class="panel-heding h3 text-center"><spring:message code="student.header"/></div>
		<div class="panel-body">
				<table class="table table-striped" id="studentHome">
					<thead>
						<tr>
							<th><spring:message code="student.table.fName"/></th>
							<th><spring:message code="student.table.lName"/></th>
							<th><spring:message code="student.table.id"/></th>
							<th><spring:message code="student.table.age"/></th>
							<th colspan="3"><spring:message code="student.table.action"/></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${students}" var="student">
							<tr>
								<td>${student.firstName}</td>
								<td>${student.lastName}</td>
								<td>${student.id}</td>
								<td>${student.age}</td>
								<td colspan="3">
									<button type="button" class="btn btn-default" onclick="getStudent(${student.id}, 'VIEW')"><spring:message code="student.btn.view"/></button>
									<button type="button" class="btn btn-primary" onclick="getStudent(${student.id}, 'EDIT')"><spring:message code="student.btn.edit"/></button>
									<button type="button" class="btn btn-danger" onclick="deleteStudent(${student.id}, this)"><spring:message code="student.btn.delete"/></button>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<div align="center">
					<button class="btn btn-info" onclick="location.href='addStudent'"><spring:message code="student.btn.add"/></button>
				</div>
		</div>
	</div>
</body>
</html>