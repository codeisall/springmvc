/**
 * 
 */

function getHref() {
	var rootPath = getContextRootPath();
	location.href = rootPath + "controller/courses";
}

function getContextRootPath(){
	var pathName = location.pathname.substring(1);
	var token = pathName.split("/");
	var rootContextPath = "/" + token[0] + "/";
	return rootContextPath;
}

function getCourse(id, mode){
	var rootPath = getContextRootPath();
	location.href = "course?id=" + id + "&mode=" + mode;
}

function deleteCourse(id) {
	var result = confirm("Do you want to delete this course?");
	if (result)
		sendDeleteRequest("./course/"+id, function() {
			window.location.href = "./courses";
		});
}

function sendDeleteRequest(url, callback){
	sendRequest(url, "", "DELETE", callback);
}

function sendRequest(url, data, method, callback){
	$.ajax({
		url : url,
		data :JSON.stringify(data),
		type : method,
		contentType : "application/json",
		success : callback,
		error : function(request, msg, error) {
		}
	});
}